CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


 INTRODUCTION
 ------------

 The Page Cache Clear module was built as a simple way to clear a page's cache
 when working with Varnish and Workbench
 Moderation module.

 Uses Rules integration to fire the action and adds a 'Ban' to Varnish.

 This module was originally developed to allow for cache clearing with Workbench
  moderation states, however it will
 work with other Rules conditions.


 REQUIREMENTS
 ------------

 * Rules (https://www.drupal.org/project/rules)
 * Varnish (https://www.drupal.org/project/varnish)


 INSTALLATION
 ------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


 CONFIGURATION
 -------------

 This module does not have any configuration settings. You instead will need to
 create a Rule that will fire the action.


 MAINTAINERS
 -----------

 Current Maintainers
 * Hayden Tapp (haydent) - https://drupal.org/user/2763191
 * Glen Ranson (GlenRanson) - https://www.drupal.org/user/2627521
